from django.urls import path, include
from . import views

app_name = 'bulletinboard'

urlpatterns = [
    path('', views.index, name='index'),
    path('init/', views.init, name='init'),
    path('index2/', views.index2, name='index2'), #temporary url
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),

    path('board/<int:id>/', views.board, name='board'), # GET request
    path('board/<str:operation>/', views.board, name='board'), # POST request
    path('board/<str:operation>/<int:id>/', views.board, name='board'), # POST request

    path('thread/<int:id>/', views.thread, name='thread'), # GET request
    path('thread2/<int:id>/', views.thread2, name='thread2'), # GET request
    path('thread/<str:operation>/', views.thread, name='thread'), # POST request
    path('thread/<str:operation>/<int:id>', views.thread, name='thread'), # POST request

    path('post/<str:operation>/', views.post, name='post'),

    path('user/<str:username>/', views.user, name='user'),
    path('user/<str:operation>/<str:username>', views.user, name='user'),
]
