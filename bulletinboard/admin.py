from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import SiteUser
# Register your models here.


class SiteUserInline(admin.StackedInline):
    model = SiteUser
    can_delete = False


class UserAdmin(BaseUserAdmin):
    inlines = (SiteUserInline,)


admin.site.unregister(User)
admin.site.register(SiteUser)
admin.site.register(User, UserAdmin)
