from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import authenticate

from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models, IntegrityError
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.utils import timezone

from .models import SiteUser, Board, Thread, Post
from .forms import RegisterForm, BoardForm, ThreadForm

# Create your views here.


# temporary function for the new templates
def index2(request):
    boards = Board.objects.order_by('position')
    context = {'boards': boards}
    return render(request, 'bulletinboard/index2.html', context)


# temporary function for the new templates
def thread2(request, id=None):
    threads = Thread.objects.all()
    context = {'threads': threads}
    return render(request, 'bulletinboard/thread2.html', context)


def init(request):

    # add custom user permissions
    content_type = ContentType.objects.get_for_model(SiteUser)
    permission = Permission.objects.create(
        codename='can_ban_site_user',
        name='Can ban site users',
        content_type=content_type,
    )
    permission = Permission.objects.create(
        codename='can_unban_site_user',
        name='Can unban site users',
        content_type=content_type,
    )

    # add custom thread permissions
    content_type = ContentType.objects.get_for_model(Thread)
    permission = Permission.objects.create(
        codename='can_lock_thread',
        name='Can lock threads',
        content_type=content_type,
    )
    permission = Permission.objects.create(
        codename='can_unlock_thread',
        name='Can unlock threads',
        content_type=content_type,
    )

    return HttpResponse('OK')


def index(request):

    context = {}
    site_user = None

    if 'user' in request.session:
        context['session'] = request.session['user']
        site_user = SiteUser.objects.get(user__username=context['session']['username'])
        print(site_user.can_add_board())

    boards = Board.objects.order_by('position')
    boards_list = []

    for board in boards:
        threads_count = board.thread_set.count()
        recent_thread = None
        threads = board.thread_set.all()

        post_count = 0
        for thread in threads:
            count = thread.post_set.count()
            post_count += count

            if count == 0:
                continue

            if recent_thread == None:
                recent_thread = thread
                continue

            if thread.post_set.order_by('-posted_on').first().posted_on > recent_thread.post_set.order_by('-posted_on').first().posted_on:
                recent_thread = thread


        boards_list.append({
            'board': board,
            'threads_count': threads_count,
            'post_count': post_count,
            'recent_thread': recent_thread,
        })


    context['data'] = boards_list
    context['site_user'] = site_user

    return render(request, 'bulletinboard/index.html', {'context': context});


def register(request):

    if request.method == 'GET':
        return render(request, 'bulletinboard/register.html')
    else:

        form = RegisterForm(request.POST)
        context = {}

        if form.is_valid():
            try:
                user = User.objects.create_user(
                    username=form.cleaned_data['username'],
                    first_name=request.POST['first_name'], last_name=request.POST['last_name'],
                    email=request.POST['email'], password=form.cleaned_data['password']
                    )

            except(IntegrityError):

                context['integrity'] = False
                context['data'] = form.cleaned_data
                return render(request, 'bulletinboard/register.html', {'context': context})
            else:
                group = Group.objects.get(name='Poster')
                user.groups.add(group)
                site_user = SiteUser(user=user, is_banned=False)
                site_user.save()
                return HttpResponseRedirect(reverse('bulletinboard:index'))
        else:
            context['form_errors'] = form.errors
            context['data'] = form.cleaned_data,
            return render(request, 'bulletinboard/register.html', {'context': context})


def login(request):

    if request.method == 'POST':
        user = authenticate(username=request.POST['username'],
                password=request.POST['password'])

        if user is not None:
            site_user = SiteUser.objects.get(user=user)

            request.session['user'] = dict()
            request.session['user']['username'] = site_user.user.username
            request.session['user']['email'] = site_user.user.email
            request.session['user']['is_banned'] = site_user.is_banned
            request.session.modified = True

        return HttpResponseRedirect(reverse('bulletinboard:index'))


def logout(request):

    if 'user' in request.session:
        del request.session['user']
        request.session.modified = True

    return HttpResponseRedirect(reverse('bulletinboard:index'))


def board(request, operation=None, id=None):

    if request.method == 'POST':
        site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
        if operation == 'create' and site_user.can_add_board():
            form = BoardForm(request.POST)

            if form.is_valid():
                board = Board(
                    name=request.POST['name'],
                    creator=site_user,
                    created_on=timezone.now(),
                    position=request.POST['position']
                    )

                board.save()

        return HttpResponseRedirect(reverse('bulletinboard:index'))

    else: # GET

        context = {}


        if 'user' in request.session:
            context['session'] = request.session['user']
            site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
            context['site_user'] = site_user

            if operation == 'remove' and site_user.can_delete_board():
                board = Board.objects.get(pk=id)
                board.delete()

                return HttpResponseRedirect(reverse('bulletinboard:index'))

        board = Board.objects.get(pk=id)
        threads = board.thread_set.order_by('-posted_on')

        thread_list = []

        for thread in threads:
            post = thread.post_set.order_by('-posted_on').first()
            thread_list.append({'thread': thread, 'post': post})

        paginator = Paginator(thread_list, 5)

        page = request.GET.get('page')
        page = page if not None else 1
        paginated_thread_list = paginator.get_page(page)

        context['data'] = {
            'board': board,
            'threads_with_recent_post': paginated_thread_list
        }


        return render(request, 'bulletinboard/board.html', {'context': context})


def thread(request, operation=None, id=None):

    if request.method == 'POST':

        site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
        if operation == 'create' and site_user.can_add_thread():
            board = Board.objects.get(pk=request.POST['board'])
            form = ThreadForm(request.POST)

            if form.is_valid():
                thread = Thread(
                    board=board,
                    title=request.POST['title'],
                    posted_by=site_user,
                    posted_on=timezone.now(),
                    is_locked=False
                )
                thread.save()

        return HttpResponseRedirect(reverse('bulletinboard:board', args=(board.id,)))

    else: # GET
        context = {}
        if 'user' in request.session:
            context['session'] = request.session['user']
            site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
            context['site_user'] = site_user

            if operation == 'lock' and site_user.can_lock_thread():

                thread = Thread.objects.get(pk=id)
                thread.is_locked = True
                thread.save()

                return HttpResponseRedirect(reverse('bulletinboard:thread', args=(id,)))

            elif operation == 'unlock' and site_user.can_unlock_thread():

                thread = Thread.objects.get(pk=id)
                thread.is_locked = False
                thread.save()

                return HttpResponseRedirect(reverse('bulletinboard:thread', args=(id,)))


        thread = Thread.objects.get(pk=id)
        posts = thread.post_set.all()

        paginator = Paginator(posts, 5)
        page = request.GET.get('page')
        page = page if page is not None else 1

        paginated_posts = paginator.get_page(page)
        context['data'] = {
            'thread': thread,
            'posts': paginated_posts,
        }


        return render(request, 'bulletinboard/thread.html', {'context': context})


def post(request, operation=None):

    if request.method == 'POST':

        site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
        if operation == 'create' and site_user.can_add_post():

            thread = Thread.objects.get(pk=request.POST['thread'])
            post = Post(thread=thread,
                message=request.POST['message'],
                posted_by=site_user,
                posted_on=timezone.now()
            )
            post.save()

        return HttpResponseRedirect(reverse('bulletinboard:thread', args=(thread.id,)))


def user(request, operation=None, username=None):

    if request.method == 'POST':
        return HttpResponseRedirect(reverse('bulletinboard:index'))
    else: # GET

        context = {}
        user_profile = SiteUser.objects.get(user__username=username)

        if 'user' in request.session:
            context['session'] = request.session['user']
            site_user = SiteUser.objects.get(user__username=request.session['user']['username'])
            context['site_user'] = site_user

            if operation == 'ban' and site_user.can_ban_site_user():
                site_user.is_banned = True
                site_user.save()

                return HttpResponseRedirect(reverse('bulletinboard:user', args=(username,)))
            elif operation == 'unban' and site_user.can_unban_site_user():
                site_user.is_banned = False
                site_user.save()

                return HttpResponseRedirect(reverse('bulletinboard:user', args=(username,)))

        posts = user_profile.post_set.order_by('-posted_on')
        paginator = Paginator(posts, 5)
        page = request.GET.get('page')
        page = page if page is not None else 1
        paginated_posts = paginator.get_page(page)

        context['data'] = {
            'posts': paginated_posts
        }

        context['user_profile'] = user_profile

        return render(request, 'bulletinboard/user.html', {'context': context})
