from django.contrib.auth.models import User, Group
from django.db import models


# Create your models here.

class SiteUser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    #role = models.ForeignKey(Role, on_delete=models.CASCADE)
    is_banned = models.BooleanField(default=False)

    def __str__(self):
        return self.user.username

    def _check_groups_permissions(self, codename):
        for group in self.user.groups.all():
            for permission in group.permissions.all():

                if permission.codename == codename:
                    return True

        return False

    def can_add_board(self):
        return self._check_groups_permissions('add_board')

    def can_delete_board(self):
        return self._check_groups_permissions('delete_board')

    def can_ban_site_user(self):
        return self._check_groups_permissions('can_ban_site_user')

    def can_unban_site_user(self):
        return self._check_groups_permissions('can_unban_site_user')

    def can_add_thread(self):
        return self._check_groups_permissions('add_thread')

    def can_lock_thread(self):
        return self._check_groups_permissions('can_lock_thread')

    def can_unlock_thread(self):
        return self._check_groups_permissions('can_unlock_thread')

    def can_add_post(self):
        return self._check_groups_permissions('add_post')


class Board(models.Model):
    name = models.CharField(max_length=100)
    creator = models.ForeignKey(SiteUser, on_delete=models.CASCADE)
    created_on = models.DateTimeField()
    position = models.PositiveSmallIntegerField(default=0)


class Thread(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    posted_by = models.ForeignKey(SiteUser, on_delete=models.CASCADE)
    posted_on = models.DateTimeField()
    is_locked = models.BooleanField(default=False)


class Post(models.Model):
    thread = models.ForeignKey(Thread, on_delete=models.CASCADE)
    message = models.TextField()
    posted_by = models.ForeignKey(SiteUser, on_delete=models.CASCADE)
    posted_on = models.DateTimeField()
