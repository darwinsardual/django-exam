from django import forms

class RegisterForm(forms.Form):
    username = forms.CharField(max_length=150)
    email = forms.EmailField()
    first_name = forms.CharField(max_length=30)
    last_name = forms.CharField(max_length=150)
    password = forms.CharField(max_length=100)

class BoardForm(forms.Form):
    name = forms.CharField(max_length=100)
    position = forms.IntegerField()

class ThreadForm(forms.Form):
    title = forms.CharField(max_length=300)
